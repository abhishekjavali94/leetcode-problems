package com.leetcode;

public class FindMedianSortedArrays {

	public static void main(String[] args) {
		int[] nums1 = { 1, 2 };
		int[] nums2 = { 3, 4 };
		System.out.println(findMedianSortedArrays(nums1, nums2));
	}

	public static double findMedianSortedArrays(int[] nums1, int[] nums2) {
		int l1 = nums1.length;
		int l2 = nums2.length;
		if (l1 > l2) {
			return findMedianSortedArrays(nums2, nums1);
		}
		int low = 0;
		int high = l1;
		while (low <= high) {
			int p1 = (low + high) / 2;
			int p2 = (l1 + l2 + 1) / 2 - p1;
			int p1l = (p1 == 0) ? Integer.MIN_VALUE : nums1[p1 - 1];
			int p1r = (p1 == l1) ? Integer.MAX_VALUE : nums1[p1];
			int p2l = (p2 == 0) ? Integer.MIN_VALUE : nums2[p2 - 1];
			int p2r = (p2 == l2) ? Integer.MAX_VALUE : nums2[p2];

			if (p1l <= p2r && p2l <= p1r) {
				if ((l1 + l2) % 2 == 0) {
					return ((double) (Math.max(p1l, p2l) + Math.min(p1r, p2r)) / 2);
				} else {
					return ((double) Math.max(p1l, p2l));
				}
			} else if (p1l > p2r) {
				high = p1 - 1;
			} else {
				low = p1 + 1;
			}
		}
		return 0d;
	}

}

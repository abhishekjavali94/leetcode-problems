package com.leetcode;

import java.util.ArrayList;
import java.util.List;

public class SimplifyPath {

	public static void main(String[] args) {
		System.out.println(simplifyPath("/a/./b/../../c/"));
	}

	public static class Node {
		String directory;
		List<Node> childNodes;
		Node parent;

		Node(String directory) {
			this.directory = directory;
			childNodes = new ArrayList<>();
		}
	}

	public static String simplifyPath(String path) {
		String[] directories = path.split("/");
		Node root = new Node("root");
		root.parent = null;
		Node currNode = root;
		for (String directory : directories) {
			if (directory.isEmpty() || directory.equals("."))
				continue;
			if (directory.equals("..")) {
				if(currNode.parent != null)
					currNode = currNode.parent;
				continue;
			}
			boolean existingDirectory = false;
			for (Node childNode : currNode.childNodes) {
				if (childNode.directory.equals(directory)) {
					currNode = childNode;
					existingDirectory = true;
					break;
				}
			}
			if (!existingDirectory) {
				Node newNode = new Node(directory);
				currNode.childNodes.add(newNode);
				newNode.parent = currNode;
				currNode = newNode;
			}
		}
		String canonicalPath = "";
		while (currNode.parent != null) {
			canonicalPath = "/" + currNode.directory + canonicalPath;
			currNode = currNode.parent;
		}
		if(canonicalPath.isEmpty()) {
			return "/";
		}
		return canonicalPath;
	}

}

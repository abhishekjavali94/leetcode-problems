package com.leetcode;

import java.util.Arrays;

import com.leetcode.Helper.ListNode;

public class RotateList {
	
	public static void main(String[] args) {
		ListNode node = Helper.create(Arrays.asList(1,2,3,4,5, 6, 7, 8));
		node = rotateRight(node, 2);
		Helper.print(node);
	}

	public static ListNode rotateRight(ListNode head, int k) {
		if(k==0) {
			return head;
		}
		int length = 0;
		ListNode node = head;
		while (node != null) {
			length++;
			node = node.next;
		}
		k = k % length;
		if(k==0) {
			return head;
		}
		ListNode p1 = head;
		for(int i = 0 ; i <= k ; i++) {
			p1 = p1.next;
		}
		ListNode p2 = head;
		while (p1 != null) {
			p1 = p1.next;
			p2 = p2.next;
		}
		ListNode newHead = p2.next;
		p2.next = null;
		ListNode p3 = newHead;
		while(p3.next != null) {
			p3 = p3.next;
		}
		p3.next = head;
		return newHead;
	}

}

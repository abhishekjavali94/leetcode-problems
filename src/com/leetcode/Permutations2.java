package com.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class Permutations2 {

	public static void main(String[] args) {
		int[] nums = new int[] { 1, 1, 2 };
		System.out.println(permuteUnique(nums));
	}

	public static List<List<Integer>> permuteUnique(int[] nums) {
		Arrays.sort(nums);
		List<List<Integer>> result = new ArrayList<>();
		permute(nums, new ArrayList<>(), result);
		return result;
	}

	public static void permute(int[] nums, List<Integer> current, List<List<Integer>> result) {
		if (nums.length == current.size()) {
			result.add(current.stream().map(curr -> nums[curr]).collect(Collectors.toList()));
			return;
		}
		int prev = 11;
		for (int i = 0; i < nums.length; i++) {
			if ((i == 0 || nums[i] != prev) && !current.contains(i)) {
				prev = nums[i];
				List<Integer> currentCopy = new ArrayList<>(current);
				currentCopy.add(i);
				permute(nums, currentCopy, result);
			}
		}
	}

}

package com.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CombinationSum {

	public static void main(String[] args) {
		int[] candidates = new int[] { 2, 3, 5 };
		int target = 8;
		List<List<Integer>> solution = combinationSum(candidates, target);
		System.out.println(solution);
	}

	public static List<List<Integer>> combinationSum(int[] candidates, int target) {
		Set<List<Integer>> solution = uniqueCombinationSum(candidates, target);
		return new ArrayList<>(solution);
	}

	public static Set<List<Integer>> uniqueCombinationSum(int[] candidates, int target) {
		Set<List<Integer>> finalSolution = new HashSet<>();
		for (int i = 0; i < candidates.length; i++) {
			if (candidates[i] > target) {
				continue;
			} else if (candidates[i] == target) {
				finalSolution.add(new ArrayList<>(Arrays.asList(candidates[i])));
			} else {
				int curr = candidates[i];
				Set<List<Integer>> solution = uniqueCombinationSum(candidates, target - curr);
				if (solution != null && !solution.isEmpty()) {
					solution.forEach(sol -> {
						sol.add(curr);
						Collections.sort(sol);
						finalSolution.add(sol);
					});
				}
			}
		}
		return finalSolution;
	}

}

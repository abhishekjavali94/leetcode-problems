package com.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ThreeSum {

	public static void main(String[] args) {
		int[] nums = { -1, 0, 1, 2, -1, -4 };
		System.out.println(threeSum(nums));
	}

	public static List<List<Integer>> threeSum(int[] nums) {
		if (nums.length == 0 || nums.length == 1 || nums.length == 2) {
			return Collections.emptyList();
		}
		List<List<Integer>> result = new ArrayList<>();
		Arrays.sort(nums);
		Set<Integer> visitedNums = new HashSet<>();
		for (int i = 0; i < nums.length - 2; i++) {
			if (visitedNums.contains(nums[i])) {
				continue;
			}
			visitedNums.add(nums[i]);
			int low = i + 1;
			int high = nums.length - 1;
			while (low < high) {
				int sum = nums[i] + nums[low] + nums[high];
				if (sum < 0) {
					low++;
				} else if (sum > 0) {
					high--;
				} else {
					result.add(Arrays.asList(nums[i], nums[low], nums[high]));
					int lowV = nums[low];
					low++;
					while (low < nums.length && nums[low] == lowV) {
						low++;
					}
					int highV = nums[high];
					high--;
					while (high > i && nums[high] == highV) {
						high--;
					}
				}
			}
		}
		return result;
	}

}

package com.leetcode;

import java.util.HashMap;
import java.util.Map;

public class TwoSum {

	public static void main(String[] args) {
		int[] input = { 2, 3, 2, 3 };
		int[] result = twoSum(input, 6);
		System.out.println("[" + result[0] + ", " + result[1] + "]");
	}

	public static int[] twoSum(int[] nums, int target) {
		Map<Integer, Integer> map = new HashMap<>();
		for (int i = 0; i < nums.length; i++) {
			map.put(nums[i], i);
		}
		int[] result = new int[2];
		for (int i = 0; i < nums.length; i++) {
			int diff = target - nums[i];
			if (map.containsKey(diff) && map.get(diff) != i) {
				result[0] = i;
				result[1] = map.get(diff);
				return result;
			}
		}
		return result;
	}

}

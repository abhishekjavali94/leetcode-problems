package com.leetcode;

public class StringToInteger {

	public static void main(String[] args) {
		System.out.println(myAtoi("42"));
		System.out.println(myAtoi("+42"));
		System.out.println(myAtoi("    -42"));
		System.out.println(myAtoi("4193 with words"));
		System.out.println(myAtoi("words and 987"));
		System.out.println(myAtoi("-91283472332"));
		System.out.println(myAtoi("+91283472332"));
		System.out.println(myAtoi("+-12"));
		System.out.println(myAtoi("+-1+-2"));
		System.out.println(myAtoi(" "));
		System.out.println(myAtoi("    "));
		System.out.println(myAtoi("9223372036854775808"));
		System.out.println(myAtoi("-9223372036854775808"));
	}

	public static int myAtoi(String s) {
		if (s.length() == 0) {
			return 0;
		}
		int i = 0;
		// skip all whitespace characters
		for (; i < s.length(); i++) {
			if (s.charAt(i) == ' ') {
				continue;
			} else {
				break;
			}
		}
		if (i == s.length()) {
			return 0;
		}
		boolean isNeg = false;
		char firstChar = s.charAt(i);
		if (firstChar == '-') {
			isNeg = true;
			i++;
		} else if (firstChar == '+') {
			i++;
		} else if (firstChar < '0' || firstChar > '9') {
			return 0;
		}
		long result = 0;
		for (; i < s.length(); i++) {
			char c = s.charAt(i);
			if (c < '0' || c > '9') {
				break;
			}
			if (isNeg) {
				result = (result * 10) - ((int) c - 48);
			} else {
				result = (result * 10) + ((int) c - 48);
			}
			if (result < Integer.MIN_VALUE) {
				return Integer.MIN_VALUE;
			} else if (result > Integer.MAX_VALUE) {
				return Integer.MAX_VALUE;
			}
		}
		return (int) result;
	}

}

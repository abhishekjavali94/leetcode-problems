package com.leetcode;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class ValidSudoku {

	public boolean isValidSudoku(char[][] board) {
		Map<Integer, Set<Integer>> rows = new HashMap<>();
		Map<Integer, Set<Integer>> columns = new HashMap<>();
		Map<Integer, Set<Integer>> boxes = new HashMap<>();
		for (int i = 0; i < 9; i++) {
			rows.put(i, new HashSet<>());
			columns.put(i, new HashSet<>());
			boxes.put(i, new HashSet<>());
		}
		for (int i = 0; i < board.length; i++) {
			for (int j = 0; j < board[i].length; j++) {
				char ch = board[i][j];
				if (ch == '.') {
					continue;
				}
				int num = Integer.getInteger(String.valueOf(ch));
				if (rows.get(i).contains(num)) {
					return false;
				}
				rows.get(i).add(num);

				if (columns.get(j).contains(num)) {
					return false;
				}
				columns.get(j).add(num);

				int boxIndex = (i / 3) * 3 + j / 3;
				if (boxes.get(boxIndex).contains(num)) {
					return false;
				}
				boxes.get(boxIndex).add(num);
			}
		}
		return true;
	}
}

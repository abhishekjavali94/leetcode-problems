package com.leetcode;

public class JumpGame {

	public static void main(String[] args) {
		int[] nums = new int[] { 3, 2, 1, 0, 4 };
		System.out.println(canJump(nums));
	}

	public static boolean canJump(int[] nums) {
		int add = 1;
		for (int i = nums.length - 2; i >= 0; i--, add++) {
			if (nums[i] >= add) {
				return true;
			}
		}
		return false;
	}

}

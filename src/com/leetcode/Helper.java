package com.leetcode;

import java.util.List;

public class Helper {

	/** LINKED LIST **/

	public static class ListNode {
		int val;
		ListNode next;

		ListNode() {
		}

		ListNode(int val) {
			this.val = val;
		}

		ListNode(int val, ListNode next) {
			this.val = val;
			this.next = next;
		}
	}

	public static ListNode insert(ListNode head, int data) {
		ListNode newNode = new ListNode(data);
		newNode.next = null;
		if (head == null) {
			head = newNode;
			return head;
		}
		ListNode last = head;
		while (last.next != null) {
			last = last.next;
		}
		last.next = newNode;
		return head;
	}

	public static ListNode create(List<Integer> values) {
		if (values == null || values.isEmpty()) {
			return null;
		}
		ListNode head = new ListNode(values.get(0));
		ListNode ptr = head;
		for (int i = 1; i < values.size(); i++) {
			ptr.next = new ListNode(values.get(i));
			ptr = ptr.next;
		}
		return head;
	}

	public static void print(ListNode head) {
		System.out.println();
		while (head != null) {
			System.out.print(head.val + " -> ");
			head = head.next;
		}
		System.out.print("null");
	}

	public static void print(int[] arr) {
		System.out.println();
		for (int i = 0; i < arr.length; i++) {
			System.out.print(arr[i] + " ");
		}
	}

}

package com.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Stack;

public class CalendarMeeting {

	public static class Slot {

		Long start;

		Long end;

		private long getTime(String timeStr) {
			int hour = Integer.valueOf(timeStr.substring(0, timeStr.indexOf(':')));
			int mins = Integer.valueOf(timeStr.substring(timeStr.indexOf(':') + 1, timeStr.length()));
			return (hour * 60) + mins;
		}

		public Slot(String start, String end) {
			this.start = getTime(start);
			this.end = getTime(end);
		}

		public Slot(long start, long end) {
			this.start = start;
			this.end = end;
		}

		private String getTime(long time) {
			return String.valueOf(time / 60).concat(":").concat(String.format("%02d", time % 60));
		}

		public String toString() {
			return "[" + getTime(start) + ", " + getTime(end) + "]";
		}

	}

	public static class Timestamp implements Comparable<Timestamp> {

		long time;

		char type;

		public Timestamp(long time, char type) {
			this.time = time;
			this.type = type;
		}

		@Override
		public int compareTo(Timestamp o) {
			if (this.time == o.time) {
				if (this.type == 'E') {
					return -1;
				} else {
					return 1;
				}
			}
			return (int) (this.time - o.time);
		}
	}

	public static void main(String[] args) {
		List<Slot> calendar1 = Arrays.asList(new Slot("9:00", "10:30"), new Slot("12:00", "13:00"),
		    new Slot("16:00", "18:00"));
		List<Slot> calendar2 = Arrays.asList(new Slot("10:00", "11:30"), new Slot("12:30", "14:30"),
		    new Slot("14:30", "15:00"), new Slot("16:00", "17:00"));
		List<Slot> emptySlots = getEmptySlots(calendar1, calendar2, new Slot("9:00", "20:00"), new Slot("10:00", "18:30"));
		System.out.println(emptySlots);
	}

	public static List<Slot> getEmptySlots(List<Slot> calendar1, List<Slot> calendar2, Slot bounds1, Slot bounds2) {
		List<Timestamp> allTimestamps = new ArrayList<>();
		allTimestamps.addAll(getTimestamps(new Slot(0, bounds1.start)));
		allTimestamps.addAll(getTimestamps(new Slot(0, bounds2.start)));
		allTimestamps.addAll(getTimestamps(new Slot(bounds1.end, 1440)));
		allTimestamps.addAll(getTimestamps(new Slot(bounds2.end, 1440)));
		calendar1.forEach(slot -> allTimestamps.addAll(getTimestamps(slot)));
		calendar2.forEach(slot -> allTimestamps.addAll(getTimestamps(slot)));
		Collections.sort(allTimestamps);

		Stack<Long> stack = new Stack<>();
		List<Slot> emptySlots = new ArrayList<>();
		long startTime = 0;
		for (Timestamp timestamp : allTimestamps) {
			if (timestamp.type == 'S') {
				if (stack.isEmpty() && startTime != 0 && startTime != timestamp.time) {
					emptySlots.add(new Slot(startTime, timestamp.time));
				}
				stack.push(timestamp.time);
			} else {
				stack.pop();
			}
			if (stack.isEmpty()) {
				startTime = timestamp.time;
			}
		}
		return emptySlots;
	}

	private static List<Timestamp> getTimestamps(Slot slot) {
		return Arrays.asList(new Timestamp(slot.start, 'S'), new Timestamp(slot.end, 'E'));
	}

}

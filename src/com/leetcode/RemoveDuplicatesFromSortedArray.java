package com.leetcode;

public class RemoveDuplicatesFromSortedArray {

	public static void main(String[] args) {
		int[] nums = { 0, 0, 1, 1, 1, 2, 2, 3, 3, 4 };
		int result = removeDuplicates(nums);
		System.out.println(result);
		for (int i = 0; i < result; i++) {
			System.out.print(" " + nums[i]);
		}
	}

	public static int removeDuplicates(int[] nums) {
		int length = nums.length;

		if (length == 0)
			return 0;

		int i;
		int targetIndex = 1;
		int OG = nums[0];

		for (i = 1; i < length; i++) {

			if (nums[i] != OG) {

				OG = nums[i];
				nums[targetIndex] = nums[i];
				targetIndex++;
			}
		}

		return targetIndex;
	}

}

package com.leetcode;

public class ImplementStrStr {

	public static void main(String[] args) {
		System.out.println(strStr("hello", "ll"));

	}

	public static int strStr(String haystack, String needle) {
		if (needle.length() == 0) {
			return 0;
		}
		if (haystack.length() == 0) {
			return -1;
		}
		for (int i = 0; i < haystack.length() - needle.length() + 1; i++) {
			if (haystack.charAt(i) == needle.charAt(0)) {
				if (needle.length() == 1) {
					return i;
				}
				boolean matches = true;
				for (int j = 1, k = i + 1; j < needle.length(); j++, k++) {
					if (needle.charAt(j) != haystack.charAt(k)) {
						matches = false;
						break;
					}
				}
				if (matches) {
					return i;
				}
			}
		}
		return -1;
	}

}

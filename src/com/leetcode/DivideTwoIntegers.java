package com.leetcode;

public class DivideTwoIntegers {

	public static void main(String[] args) {
		System.out.println(Integer.MAX_VALUE);
		System.out.println(Integer.MIN_VALUE);
		// System.out.println(divide(10, 5));
		// System.out.println(divide(10, 3));
		// System.out.println(divide(0, 1));
		// System.out.println(divide(1, 1));
		System.out.println(divide(-10, -5));
		// System.out.println(divide(Integer.MAX_VALUE, 1));
		// System.out.println(divide(Integer.MIN_VALUE, 1));
		// System.out.println(divide(Integer.MAX_VALUE, -1));
		System.out.println(divide(Integer.MIN_VALUE, -1));
	}

	public static int divide(int dividend, int divisor) {
		if (dividend == 0)
			return 0;
		if (Integer.MIN_VALUE == dividend && divisor == -1) {
			return Integer.MAX_VALUE;
		}
		int D = Math.abs(dividend);
		int d = Math.abs(divisor);
		int res = 0;
		if (D - d >= 0) {
			int x = 0;
			while (D - (d >> 1 >> x) >= 0) {
				x++;
			}
			res += 1 << x;
			D -= d << x;
		}
		return (dividend >= 0) == (divisor >= 0) ? res : res * -1;
	}

}

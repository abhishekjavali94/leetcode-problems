package com.leetcode;

import java.util.ArrayList;
import java.util.List;

public class Permutations {

	public static void main(String[] args) {
		int[] nums = new int[] { 1, 2, 3 };
		System.out.println(permute(nums));
	}

	public static List<List<Integer>> permute(int[] nums) {
		List<List<Integer>> result = new ArrayList<>();
		permute(nums, new ArrayList<>(), result);
		return result;
	}

	public static void permute(int[] nums, List<Integer> current, List<List<Integer>> result) {
		if (nums.length == current.size()) {
			result.add(current);
			return;
		}
		for (int i = 0; i < nums.length; i++) {
			if (!current.contains(nums[i])) {
				List<Integer> currentCopy = new ArrayList<>(current);
				currentCopy.add(nums[i]);
				permute(nums, currentCopy, result);
			}
		}
	}

}

package com.leetcode;

import static com.leetcode.Helper.insert;

import java.util.Comparator;
import java.util.PriorityQueue;

import com.leetcode.Helper.ListNode;

public class MergeKSortedLists {

	public static ListNode mergeKLists(ListNode[] lists) {
		if (lists.length == 0) {
			return null;
		}
		Comparator<ListNode> cmp = new Comparator<ListNode>() {
			@Override
			public int compare(ListNode o1, ListNode o2) {
				// TODO Auto-generated method stub
				return o1.val - o2.val;
			}
		};
		PriorityQueue<ListNode> pq = new PriorityQueue<>(cmp);
		for (ListNode l : lists) {
			if (l != null) {
				pq.add(l);
			}
		}
		ListNode head = null;
		while (true) {
			ListNode node = pq.peek();
			if (node == null) {
				break;
			}
			head = insert(head, node.val);
			pq.add(node.next);
		}
		return head;
	}

}

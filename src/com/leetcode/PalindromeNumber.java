package com.leetcode;

public class PalindromeNumber {

	public static void main(String[] args) {
		System.out.println(isPalindrome(121));
		System.out.println(isPalindrome(1551));
		System.out.println(isPalindrome(3223));
		System.out.println(isPalindrome(3229223));
		System.out.println(isPalindrome(1261));
		System.out.println(isPalindrome(1000021));
		System.out.println(isPalindrome(11));
	}

	public static boolean isPalindrome(int x) {
		if (x < 0 || (x % 10 == 0 && x != 0)) {
			return false;
		}
		int revertedNumber = 0;
		while (x > revertedNumber) {
			revertedNumber = revertedNumber * 10 + x % 10;
			x /= 10;
		}
		return x == revertedNumber || x == revertedNumber / 10;
	}

}

package com.leetcode;

import java.util.Arrays;

import com.leetcode.Helper.ListNode;

public class ReverseNodesInKGroup {

	public static void main(String[] args) {
		ListNode l = Helper.create(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9));
		Helper.print(l);
		l = reverseKGroup(l, 3);
		Helper.print(l);
	}

	public static ListNode reverseKGroup(ListNode head, int k) {
		ListNode root = new ListNode(0, head);
		ListNode curr = head;
		ListNode prev = root;

		while (curr != null) {
			ListNode tail = curr;
			int listIndex = 0;
			while (curr != null && listIndex < k) {
				curr = curr.next;
				listIndex++;
			}
			if (listIndex != k) {
				prev.next = tail;
			} else {
				prev.next = reverse(tail, k);
				prev = tail;
			}
		}
		return root.next;
	}

	public static ListNode reverse(ListNode head, int k) {
		ListNode prev = null;
		ListNode current = head;

		while (current != null && k-- > 0) {
			ListNode next = current.next;
			current.next = prev;
			prev = current;
			current = next;
		}
		head = prev;
		return prev;
	}

}

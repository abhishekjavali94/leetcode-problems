package com.leetcode;

public class MinimumPathSum {

	public static void main(String[] args) {
		int[][] grid = new int[][] { { 1, 3, 1 }, { 1, 5, 1 }, { 4, 2, 1 } };
		System.out.println(minPathSum(grid));
	}

	public static int minPathSum(int[][] grid) {
		int m = grid.length;
		int n = grid[0].length;
		return minPathSum(grid, m, n, 0, 0, new Integer[m][n]);
	}

	public static int minPathSum(int[][] grid, int m, int n, int x, int y, Integer[][] memory) {
		if (x == m - 1 && y == n - 1) {
			return grid[x][y];
		}
		if (memory[x][y] != null) {
			return memory[x][y];
		}
		int rightPathWeight = Integer.MAX_VALUE;
		if (x + 1 < m) {
			rightPathWeight = minPathSum(grid, m, n, x + 1, y, memory);
		}
		int downPathWeight = Integer.MAX_VALUE;
		if (y + 1 < n) {
			downPathWeight = minPathSum(grid, m, n, x, y + 1, memory);
		}
		int result = grid[x][y] + Math.min(rightPathWeight, downPathWeight);
		memory[x][y] = result;
		return result;
	}

}

package com.leetcode;

public class Search2DMatrix {

	public boolean searchMatrix(int[][] matrix, int target) {
		if (target < matrix[0][0]) {
			return false;
		}
		for (int i = 0; i < matrix.length; i++) {
			if (target == matrix[i][0]) {
				return true;
			} else if (matrix[i][0] > target) {
				return binSearch(matrix[i - 1], target, 0, matrix[0].length);
			}
		}
		return false;
	}

	public boolean binSearch(int[] arr, int target, int low, int upper) {
		int mid = (low + upper) / 2;
		if (arr[mid] == target) {
			return true;
		}
		if (arr[mid] > target) {
			return binSearch(arr, target, low, mid - 1);
		}
		return binSearch(arr, target, mid + 1, upper);
	}

}

package com.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PhoneNumberLetterCombinations {

	public static void main(String[] args) {
		System.out.println(letterCombinations("2345"));
	}

	static Map<Character, List<Character>> phone = new HashMap<Character, List<Character>>() {
		{
			put('2', Arrays.asList('a', 'b', 'c'));
			put('3', Arrays.asList('d', 'e', 'f'));
			put('4', Arrays.asList('g', 'h', 'i'));
			put('5', Arrays.asList('j', 'k', 'l'));
			put('6', Arrays.asList('m', 'n', 'o'));
			put('7', Arrays.asList('p', 'q', 'r', 's'));
			put('8', Arrays.asList('t', 'u', 'v'));
			put('9', Arrays.asList('w', 'x', 'y', 'z'));
		}
	};

	static List<String> result = new ArrayList<>();

	public static List<String> letterCombinations(String digits) {
		if (digits.length() != 0) {
			func(digits, "");
		}
		return result;
	}

	public static void func(String digits, String pre) {
		if (digits == null || digits.isEmpty() || "".equals(digits)) {
			result.add(pre);
			return;
		}
		char c = digits.charAt(0);
		for (Character ch : phone.get(c)) {
			func(digits.substring(1), new StringBuilder(pre).append(ch).toString());
		}
	}

}

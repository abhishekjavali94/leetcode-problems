package com.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class PascalTriangle {

	public static void main(String[] args) {
		print(generate(5));

	}

	public static List<List<Integer>> generate(int numRows) {
		if (numRows == 0) {
			return Collections.emptyList();
		}
		List<List<Integer>> result = new ArrayList<>();
		result.add(Arrays.asList(1));
		for (int i = 1; i < numRows; i++) {
			List<Integer> row = new ArrayList<>();
			row.add(1);
			List<Integer> parentRow = result.get(i - 1);
			for (int j = 0; j < parentRow.size() - 1; j++) {
				row.add(parentRow.get(j) + parentRow.get(j + 1));
			}
			row.add(1);
			result.add(row);
		}
		return result;
	}

	public static void print(List<List<Integer>> triangle) {
		triangle.forEach(row -> {
			System.out.println(row);
		});
	}

}

package com.leetcode;

public class SetMatrixZeros {

	public void setZeroes(int[][] matrix) {
		for(int i = 0 ; i < matrix.length ; i++) {
			for(int j = 0 ; j < matrix[0].length ; j++) {
				if(matrix[i][j] == 0) {
					matrix[i][0] = 0;
					matrix[0][j] = 0;
				}
			}
		}
		for(int i = 0 ; i < matrix.length ; i++) {
			if(matrix[i][0] == 0) {
				for(int j = 0 ; j < matrix[0].length ; j++) {
					matrix[i][j] = 0;
				}
			}
		}

	}
	
	

}

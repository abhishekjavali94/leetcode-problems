package com.leetcode;

public class LongestPalindromeSubstring {

	public static void main(String[] args) {
		System.out.println(longestPalindrome("babad"));
		System.out.println(longestPalindrome("cbbd"));
		System.out.println(longestPalindrome("a"));
		System.out.println(longestPalindrome("ac"));
		System.out.println(longestPalindrome("aabaaca"));
	}

	public static String longestPalindrome(String s) {
		if (s.length() == 1) {
			return s;
		}
		if (s.length() == 2) {
			if (s.charAt(0) == s.charAt(1)) {
				return s;
			}
			return String.valueOf(s.charAt(0));
		}
		boolean[][] matrix = new boolean[s.length()][s.length()];
		for (int i = 0, j = 0; i < s.length(); i++, j++) {
			matrix[i][j] = true;
		}
		for (int j = 0, i = 1; i < s.length(); i++, j++) {
			if (s.charAt(i) == s.charAt(j)) {
				matrix[i][j] = true;
			}
		}
		for (int i = 2; i < s.length(); i++) {
			for (int j = 0; j < i - 1; j++) {
				if (s.charAt(i) == s.charAt(j) && matrix[i - 1][j + 1]) {
					matrix[i][j] = true;
				}
			}
		}
		int[] result = new int[2];
		int min = Integer.MIN_VALUE;
		for (int i = 0; i < s.length(); i++) {
			for (int j = 0; j < s.length(); j++) {
				int diff = Math.abs(j - i);
				if (matrix[i][j] && diff > min) {
					min = diff;
					result[0] = i;
					result[1] = j;
				}
			}
		}
		if(result[0] > result[1]) {
			return s.substring(result[1], result[0] + 1);
		}
		return s.substring(result[0], result[1] + 1);
	}

}

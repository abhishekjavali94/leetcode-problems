package com.leetcode;

import java.util.HashSet;
import java.util.Set;

public class EvaulateMathematicalExpression {

	public static class Node {

		String value;

		Node left;

		Node right;

		Node(String value) {
			this.value = value;
		}
	}

	public static void main(String[] args) {
		// System.out.println(evaulateExpression("5+4/2+6/3"));
		System.out.println(evaulateExpression("5+4/2+6/3*10/5+99-100-50+40*90"));
		// System.out.println(evaulateExpression("5+4/2+6/3*10/5+99"));
		// System.out.println(evaulateExpression("5+4/2+6"));
	}

	public static double evaulateExpression(String exp) {
		Set<Character> operandChars = new HashSet<>();
		operandChars.add('+');
		operandChars.add('-');
		operandChars.add('*');
		operandChars.add('/');
		if (exp.length() == 0) {
			return 0;
		}
		Node root = new Node("+");
		root.left = new Node("0");
		int index = 0;
		char ch = exp.charAt(index++);
		StringBuilder num = new StringBuilder();
		while (!operandChars.contains(ch)) {
			num.append(ch);
			ch = exp.charAt(index++);
		}
		root.right = new Node(num.toString());
		Character prevOperator = null;
		num = new StringBuilder();
		for (int i = index - 1; i < exp.length(); i++) {
			ch = exp.charAt(i);
			if (operandChars.contains(ch)) {
				if (prevOperator != null) {
					if(prevOperator == '-') {
						root = insertNode(root, '+', "-".concat(num.toString()));
					} else {
						root = insertNode(root, prevOperator, num.toString());
					}
					num = new StringBuilder();
				}
				prevOperator = ch;
			} else {
				num.append(ch);
			}
		}
		root = insertNode(root, prevOperator, num.toString());
		printTree(root);
		System.out.println();
		return evaluateTree(root);
	}

	private static double evaluateTree(Node node) {
		if (node.left == null && node.right == null) {
			return Double.valueOf(node.value);
		}
		double leftEval = evaluateTree(node.left);
		double rightEval = evaluateTree(node.right);
		System.out.println(leftEval + " " + node.value + " " + rightEval);
		switch (node.value) {
		case "+":
			return leftEval + rightEval;
		case "*":
			return leftEval * rightEval;
		case "/":
			return leftEval / rightEval;
		}
		return 0;
	}

	private static int getPrecedence(char ch) {
		switch (ch) {
		case '+':
			return 1;
		case '*':
			return 3;
		case '/':
			return 4;
		}
		return 0;
	}

	private static Node insertNode(Node root, char operator, String operand) {
		Node prev = null;
		Node n = root;
		while (true) {
			if (n.left == null && n.right == null) {
				String nValue = n.value;
				n.value = String.valueOf(operator);
				n.left = new Node(nValue);
				n.right = new Node(operand);
				return root;
			} else if (getPrecedence(operator) >= getPrecedence(n.value.charAt(0))) {
				prev = n;
				n = n.right;
			} else {
				prev.right = new Node(String.valueOf(operator));
				prev.right.right = new Node(operand);
				prev.right.left = n;
				return root;
			}
		}
	}

	private static void printTree(Node root) {
		if (root.left == null && root.right == null) {
			System.out.print(root.value + " ");
			return;
		}
		printTree(root.left);
		System.out.print(root.value + " ");
		printTree(root.right);
	}
}

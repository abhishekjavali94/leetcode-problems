package com.leetcode;

import static com.leetcode.Helper.insert;

import java.util.Objects;

import com.leetcode.Helper.ListNode;

public class AddTwoNumbers {

	public static void main(String[] args) {
		ListNode l1 = new ListNode(9,
		    new ListNode(9, new ListNode(9, new ListNode(9, new ListNode(9, new ListNode(9, new ListNode(9, null)))))));
		ListNode l2 = new ListNode(9, new ListNode(9, new ListNode(9, new ListNode(9, null))));
		// ListNode l1 = new ListNode(2, new ListNode(4, new ListNode(3, null)));
		// ListNode l2 = new ListNode(5, new ListNode(6, new ListNode(4, null)));
		ListNode result = addTwoNumbers(l1, l2);
		ListNode next = result;
		while (Objects.nonNull(next)) {
			System.out.print(next.val + " -> ");
			next = next.next;
		}
		System.out.print("null");
	}

	public static ListNode addTwoNumbers(ListNode l1, ListNode l2) {
		ListNode result = null;
		ListNode l1Node = l1;
		ListNode l2Node = l2;
		int carry = 0;
		while (l1Node != null || l2Node != null) {
			int sum = carry;
			if (l1Node != null) {
				sum += l1Node.val;
				l1Node = l1Node.next;
			}
			if (l2Node != null) {
				sum += l2Node.val;
				l2Node = l2Node.next;
			}
			if (sum >= 10) {
				carry = 1;
				sum %= 10;
			} else {
				carry = 0;
			}
			result = insert(result, sum);
		}
		if (carry != 0) {
			result = insert(result, carry);
		}
		return result;
	}

}

package com.leetcode;

import java.util.ArrayList;
import java.util.List;

public class MergeIntervals {

	public static void main(String[] args) {
		int[][] intervals = new int[][] { { 1, 4 }, { 1, 5 }, { 6, 9 }, { 5, 9 }, { 8, 10 }, { 15, 18 } };
		int[][] result = merge(intervals);
		for (int i = 0; i < result.length; i++) {
			System.out.print("[" + result[i][0] + "," + result[i][1] + "] ");
		}
	}

	public enum Position {
		START, END;
	}

	public static int[][] merge(int[][] intervals) {
		int min = Integer.MAX_VALUE;
		int max = Integer.MIN_VALUE;
		for (int i = 0; i < intervals.length; i++) {
			min = Math.min(min, intervals[i][0]);
			min = Math.min(min, intervals[i][1]);
			max = Math.max(max, intervals[i][0]);
			max = Math.max(max, intervals[i][1]);
		}
		boolean[] visited = new boolean[max - min + 1];
		for (int i = 0; i < intervals.length; i++) {
			for (int j = intervals[i][0]; j <= intervals[i][1]; j++) {
				visited[j - min] = true;
			}
		}
		List<int[]> result = new ArrayList<>();
		int start = min;
		boolean isVisited = true;
		for (int i = 1; i < visited.length; i++) {
			if (visited[i]) {
				if (!isVisited) {
					start = i + min;
				}
			} else {
				if (isVisited) {
					result.add(new int[] { start, i });
					isVisited = false;
				}
			}
		}
		result.add(new int[]{start, max});
		return result.toArray(new int[result.size()][]);
	}

}

package com.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FourSum {

	public static void main(String[] args) {
		int[] nums = { 1, 0, -1, 0, -2, 2 };
		System.out.println(fourSum(nums, 0));
	}

	public static List<List<Integer>> fourSum(int[] nums, int target) {
		if (nums.length == 0 || nums.length == 1 || nums.length == 2) {
			return Collections.emptyList();
		}
		List<List<Integer>> result = new ArrayList<>();
		Arrays.sort(nums);
		Set<Integer> visitedNums1 = new HashSet<>();
		for (int i = 0; i < nums.length - 3; i++) {
			if (visitedNums1.contains(nums[i])) {
				continue;
			}
			visitedNums1.add(nums[i]);
			Set<Integer> visitedNums2 = new HashSet<>();
			for (int j = i + 1; j < nums.length - 2; j++) {
				if (visitedNums2.contains(nums[j])) {
					continue;
				}
				visitedNums2.add(nums[j]);
				int low = j + 1;
				int high = nums.length - 1;
				while (low < high) {
					int sum = nums[i] + nums[j] + nums[low] + nums[high];
					if (sum < target) {
						low++;
					} else if (sum > target) {
						high--;
					} else {
						result.add(Arrays.asList(nums[i], nums[j], nums[low], nums[high]));
						int lowV = nums[low];
						low++;
						while (low < nums.length && nums[low] == lowV) {
							low++;
						}
						int highV = nums[high];
						high--;
						while (high > j && nums[high] == highV) {
							high--;
						}
					}
				}
			}
		}
		return result;
	}

}

package com.quinbay;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

public class AssignStudents {
	
	public enum Location {
		MUMBAI, DELHI, HYDERABAD;
	}

	public static class Student implements Comparable<Student> {
		public String firstName;
		public String lastName;
		public int grade;
		public Map<String, Double> marks;
		public Character section;
		public boolean admissionGranted;
		
		public List<Location> locationPreference;
		public Location locationAllocated;

		public Double average;

		@Override
		public int compareTo(Student o) {
			return o.average.compareTo(this.average);
		}

		public Student(String firstName, String lastName, int grade, List<Double> marks) {
			this.firstName = firstName;
			this.lastName = lastName;
			this.grade = grade;
			this.marks = new HashMap<>();
			marks.forEach(mark -> {
				this.marks.put(UUID.randomUUID().toString(), mark);
			});
		}

		@Override
		public String toString() {
			return "Student [firstName=" + firstName + ", lastName=" + lastName + ", grade=" + grade + ", section=" + section
			    + ", admissionGranted=" + admissionGranted + ", average=" + average + "]";
		}

	}

	public static void main(String[] args) {
		List<Student> students = new ArrayList<>();
		students.add(new Student("Harry", "Potter", 3, Arrays.asList(80D, 80D, 80D)));
		students.add(new Student("Hermoine", "Granger", 3, Arrays.asList(100D, 100D, 100D)));
		students.add(new Student("Ron", "Weasley", 3, Arrays.asList(50D, 50D, 50D)));
		students.add(new Student("Neville", "Longbottom", 2, Arrays.asList(70D, 70D, 70D)));
		students.add(new Student("Draco", "Malfoy", 3, Arrays.asList(20D, 20D, 20D)));
		students = assignAdmission(students, 2);
		for (Student stu : students) {
			System.out.println(stu);
		}

	}

	public static List<Student> assignAdmission(List<Student> students, int limit) {
		Map<Integer, List<Student>> groupedStudents = new HashMap<>();
		List<Student> finalStudents = new ArrayList<>();
		Map<Location, Map<Integer, Integer>> locationPreferenceCount = new EnumMap<>(Location.class);
		Arrays.asList(Location.values()).stream().forEach(location -> {
			locationPreferenceCount.put(location, new HashMap<>());
		});
		students.forEach(student -> {
			student.average = student.marks.values().stream().collect(Collectors.summingDouble(Double::doubleValue))
			    / student.marks.size();
			if (student.average < 35) {
				student.admissionGranted = false;
				finalStudents.add(student);
			} else {
				student.admissionGranted = true;
				if (!groupedStudents.containsKey(student.grade))
					groupedStudents.put(student.grade, new ArrayList<>());
				groupedStudents.get(student.grade).add(student);
				for(Location location : student.locationPreference) {
					if(!locationPreferenceCount.get(location).containsKey(student.grade)) {
						locationPreferenceCount.get(location).put(student.grade, 1);
						student.locationAllocated = location;
						break;
					}
					
				}
			}
		});
		for (Map.Entry<Integer, List<Student>> entry : groupedStudents.entrySet()) {
			Collections.sort(entry.getValue());
			int count = 0;
			char section = 'A';
			for (Student stu : entry.getValue()) {
				count++;
				stu.section = section;
				if (count == limit) {
					count = 0;
					section++;
				}
				finalStudents.add(stu);
				//allocate location
			}
		}
		return finalStudents;
	}

}

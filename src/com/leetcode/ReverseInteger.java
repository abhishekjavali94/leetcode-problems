package com.leetcode;

public class ReverseInteger {

	public static void main(String[] args) {
		System.out.println(reverse(123));
		System.out.println(reverse(-123));
		System.out.println(reverse(120));
		System.out.println(reverse(0));
		System.out.println(reverse(1534236469));
	}

	public static int reverse(int x) {
		int abs = Math.abs(x);
		String str = String.valueOf(abs);
		String rev = reverse(str);
		int revInt;
		try {
			revInt = Integer.valueOf(rev);
		} catch (NumberFormatException e) {
			return 0;
		}
		if (x < 0) {
			revInt *= -1;
		}
		return revInt;

	}

	private static String reverse(String str) {
		StringBuilder rev = new StringBuilder();
		for (int i = str.length() - 1; i >= 0; i--) {
			rev.append(str.charAt(i));
		}
		return rev.toString();
	}

}

package com.leetcode;

public class NextPermutation {

	public static void main(String[] args) {
		int[] nums = { 9, 4, 2, 1 };
		nextPermutation(nums);
		Helper.print(nums);
	}

	public static void nextPermutation(int[] nums) {
		if (nums.length == 1) {
			return;
		}
		int i = nums.length - 1;
		for (; i > 0; i--) {
			if (nums[i - 1] < nums[i])
				break;
		}
		if (i == 0) {
			reverse(nums);
			return;
		}
		i--;
		int t = nums[i];
		int minDiff = Integer.MAX_VALUE;
		int swapIndex = i;
		for (int j = i + 1; j < nums.length; j++) {
			int diff = nums[j] - t;
			if (diff > 0 && diff < minDiff) {
				minDiff = diff;
				swapIndex = j;
			}
		}
		nums[i] = nums[swapIndex];
		nums[swapIndex] = t;
		i++;
		sort(nums, i);
	}

	public static void reverse(int[] nums) {
		for (int i = 0, j = nums.length - 1; i < nums.length / 2; i++, j--) {
			int temp = nums[j];
			nums[j] = nums[i];
			nums[i] = temp;
		}
	}

	public static void sort(int[] nums, int start) {
		for (int i = start; i < nums.length - 1; i++) {
			for (int j = i + 1; j < nums.length; j++) {
				if (nums[i] > nums[j]) {
					int temp = nums[j];
					nums[j] = nums[i];
					nums[i] = temp;
				}
			}
		}

	}
}

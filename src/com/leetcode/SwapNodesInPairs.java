package com.leetcode;

import com.leetcode.Helper.ListNode;

public class SwapNodesInPairs {

	public ListNode swapPairs(ListNode head) {
		ListNode temp = new ListNode(0);
		temp.next = head;
		ListNode d = temp;

		while (d.next != null && d.next.next != null) {
			ListNode p = d.next;
			ListNode q = d.next.next;
			d.next = q;
			p.next = q.next;
			q.next = p;
			d = p;
		}

		return temp.next;
	}

}

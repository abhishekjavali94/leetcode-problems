package com.leetcode;

import java.util.HashMap;
import java.util.Map;

public class LongestCommonPrefix {

	public static void main(String[] args) {
		String[] strs = { "", "", "" };
		System.out.println(longestCommonPrefix(strs));
		String[] strs1 = { "abcd", "abcd", "abcd" };
		System.out.println(longestCommonPrefix(strs1));
	}

	public static String longestCommonPrefix(String[] strs) {
		if (strs.length == 0) {
			return "";
		}
		String minString = strs[0];
		for (String str : strs) {
			if (str.length() < minString.length()) {
				minString = str;
			}
		}
		int i = 0;
		for (; i < minString.length(); i++) {
			char c = minString.charAt(i);
			boolean flag = true;
			for (String str : strs) {
				if (str.charAt(i) != c) {
					flag = false;
					break;
				}
			}
			if (!flag) {
				break;
			}
		}
		if (i == 0) {
			return "";
		}
		return minString.substring(0, i);
	}

}

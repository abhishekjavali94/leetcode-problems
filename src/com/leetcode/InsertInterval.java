package com.leetcode;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class InsertInterval {

	public static void main(String[] args) {
		int[][] intervals = new int[][] { { 1, 5 } };
		int[] newInterval = new int[] { 6, 8 };
		int[][] result = insert(intervals, newInterval);
		for (int i = 0; i < result.length; i++) {
			System.out.print("[" + result[i][0] + "," + result[i][1] + "] ");
		}
	}

	public static int[][] insert(int[][] intervals, int[] newInterval) {
		// insert newInterval at correct index
		List<int[]> newIntervals = new ArrayList<>();
		boolean newIntervalInserted = false;
		for (int[] interval : intervals) {
			if (newInterval[0] <= interval[0]) {
				newIntervals.add(newInterval);
				newIntervalInserted = true;
			}
			newIntervals.add(interval);
		}
		if (!newIntervalInserted) {
			newIntervals.add(newInterval);
		}
		return merge(newIntervals);
	}

	public static int[][] merge(List<int[]> intervals) {
		LinkedList<int[]> merged = new LinkedList<>();
		for (int[] interval : intervals) {
			// if the list of merged intervals is empty or if the current
			// interval does not overlap with the previous, simply append it.
			if (merged.isEmpty() || merged.getLast()[1] < interval[0]) {
				merged.add(interval);
			}
			// otherwise, there is overlap, so we merge the current and previous
			// intervals.
			else {
				merged.getLast()[1] = Math.max(merged.getLast()[1], interval[1]);
			}
		}
		return merged.toArray(new int[merged.size()][]);
	}

}

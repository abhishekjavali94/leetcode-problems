package com.leetcode;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class ThreeSumClosest {

	public static void main(String[] args) {
		int[] nums = { 1, 1, -1, -1, 3 };
		System.out.println(threeSumClosest(nums, -1));
	}

	public static int threeSumClosest(int[] nums, int target) {
		int diff = Integer.MAX_VALUE;
		Arrays.sort(nums);
		Set<Integer> visitedNums = new HashSet<>();
		for (int i = 0; i < nums.length - 2; i++) {
			if (visitedNums.contains(nums[i])) {
				continue;
			}
			int low = i + 1;
			int high = nums.length - 1;
			while (low < high) {
				int sum = nums[i] + nums[low] + nums[high];
				if (Math.abs(target - sum) < Math.abs(diff)) {
					diff = target - sum;
				}
				if (sum < target) {
					low++;
				} else if (sum > target) {
					high--;
				} else {
					low++;
					high--;
				}
			}
		}
		return target - diff;
	}
}

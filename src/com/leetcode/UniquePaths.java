package com.leetcode;

public class UniquePaths {

	public static void main(String[] args) {
		System.out.println(uniquePaths(3, 7));
		System.out.println(uniquePaths(3, 2));
	}

	public static int uniquePaths(int m, int n) {
		Integer[][] memory = new Integer[m][n];
		return uniquePaths(m, n, 0, 0, memory);
	}

	public static int uniquePaths(int m, int n, int x, int y, Integer[][] memory) {
		if (x == m - 1 && y == n - 1) {
			return 1;
		}
		if (memory[x][y] != null) {
			return memory[x][y];
		}
		int rightPaths = 0;
		int downPaths = 0;
		if (x + 1 < m) {
			rightPaths = uniquePaths(m, n, x + 1, y, memory);
			memory[x + 1][y] = rightPaths;
		}
		if (y + 1 < n) {
			downPaths = uniquePaths(m, n, x, y + 1, memory);
			memory[x][y + 1] = downPaths;
		}
		return rightPaths + downPaths;
	}

}

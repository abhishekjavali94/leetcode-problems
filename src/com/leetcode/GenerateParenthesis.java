package com.leetcode;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class GenerateParenthesis {

	public static void main(String[] args) {
		List<String> myResult = generateParenthesis(4);
		Collections.sort(myResult);
		List<String> expectedResult = Arrays.asList("(((())))", "((()()))", "((())())", "((()))()", "(()(()))", "(()()())",
		    "(()())()", "(())(())", "(())()()", "()((()))", "()(()())", "()(())()", "()()(())", "()()()()");
		Collections.sort(expectedResult);
		System.out.println(expectedResult);
		System.out.println(myResult);
	}

	public static Set<String> result = new HashSet<>();

	public static List<String> generateParenthesis(int n) {
		func(n, n, "");
		return result.stream().collect(Collectors.toList());
	}

	public static void func(int left, int right, String str) {
		if (left == 0 && right == 0) {
			result.add(str);
			return;
		}
		if (left != 0) {
			func(left - 1, right, new StringBuilder(str).append("(").toString());
		}
		if (right != 0 && left < right) {
			func(left, right - 1, new StringBuilder(str).append(")").toString());
		}
	}

}

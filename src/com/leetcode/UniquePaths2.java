package com.leetcode;

public class UniquePaths2 {

	public static int uniquePathsWithObstacles(int[][] obstacleGrid) {
		int m = obstacleGrid.length;
		int n = obstacleGrid[0].length;
		return uniquePathsWithObstacles(obstacleGrid, m, n, 0, 0, new Integer[m][n]);

	}

	public static int uniquePathsWithObstacles(int[][] obstacleGrid, int m, int n, int x, int y, Integer[][] memory) {
		if (obstacleGrid[x][y] == 1) {
			return 0;
		}
		if (x == m - 1 && y == n - 1) {
			return 1;
		}
		if (memory[x][y] != null) {
			return memory[x][y];
		}
		int rightPath = 0;
		if (x + 1 < m) {
			rightPath = uniquePathsWithObstacles(obstacleGrid, m, n, x + 1, y, memory);
			memory[x + 1][y] = rightPath;
		}
		int downPath = 0;
		if (y + 1 < n) {
			downPath = uniquePathsWithObstacles(obstacleGrid, m, n, x, y + 1, memory);
			memory[x][y + 1] = downPath;
		}
		return rightPath + downPath;

	}

}

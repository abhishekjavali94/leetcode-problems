package com.leetcode;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class CombinationSum2 {

	public static void main(String[] args) {
		int[] candidates = new int[] { 10,1,2,7,6,1,5 };
		int target = 8;
		List<List<Integer>> solution = combinationSum2(candidates, target);
		System.out.println(solution);
	}

	public static List<List<Integer>> combinationSum2(int[] candidates, int target) {
		List<List<Integer>> result = new ArrayList<>();
		Arrays.sort(candidates);
		return null;
	}

	public static Set<List<Integer>> uniqueCombinationSum(int[] candidates, List<Integer> indexesToIgnore, int target) {
		Set<List<Integer>> finalSolution = new HashSet<>();
		for (int i = 0; i < candidates.length; i++) {
			if (candidates[i] > target || indexesToIgnore.contains(i)) {
				continue;
			} else if (candidates[i] == target) {
				finalSolution.add(new ArrayList<>(Arrays.asList(candidates[i])));
			} else {
				int curr = candidates[i];
				List<Integer> newIndexesToIgnore = new ArrayList<>(indexesToIgnore);
				newIndexesToIgnore.add(i);
				Set<List<Integer>> solution = uniqueCombinationSum(candidates, newIndexesToIgnore, target - curr);
				if (!solution.isEmpty()) {
					solution.forEach(sol -> {
						sol.add(curr);
						finalSolution.add(sol);
					});
				}
			}
		}
		return finalSolution;
	}

}

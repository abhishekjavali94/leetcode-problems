package com.leetcode;

public class SearchInRotatedSortedArray {

	public static void main(String[] args) {
		// int[] nums = { 4, 5, 6, 7, 0, 1, 2 };
		// System.out.println(search(nums, 0));
		int[] nums = { 1, 3 };
		System.out.println(search(nums, 3));
	}

	public static int search(int[] nums, int target) {
		int start = 0;
		int end = nums.length - 1;
		while (start < end) {
			int middle = (start + end) / 2;
			if (nums[middle] == target) {
				return middle;
			}
			if (nums[middle] >= nums[start]) {
				if (target <= nums[middle] && target >= nums[start]) {
					end = middle - 1;
				} else {
					start = middle + 1;
				}
			} else {
				if (target >= nums[middle] && target <= nums[end]) {
					start = middle + 1;
				} else {
					end = middle - 1;
				}
			}
		}
		if (start == end && nums[start] == target) {
			return start;
		}
		return -1;
	}

}

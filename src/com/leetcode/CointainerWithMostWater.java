package com.leetcode;

public class CointainerWithMostWater {

	public static void main(String[] args) {
		int[] height = { 4, 3, 2, 1, 4 };
		System.out.println(maxArea(height));
	}

	public static int maxArea(int[] height) {
		int max = Integer.MIN_VALUE;
		int i = 0, j = height.length - 1;
		while (i < j) {
			max = Math.max(max, Math.min(height[i], height[j]) * (j - i));
			if (height[i] < height[j]) {
				i++;
			} else {
				j--;
			}
		}
		return max;
	}

}

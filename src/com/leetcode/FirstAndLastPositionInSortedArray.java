package com.leetcode;

public class FirstAndLastPositionInSortedArray {

	public static void main(String[] args) {
		Helper.print(searchRange(new int[] { 5, 7, 7, 8, 8, 10 }, 8));
	}

	public static int[] searchRange(int[] nums, int target) {
		int left = 0;
		int right = nums.length - 1;
		while (left < right) {
			int mid = (left + right) / 2;
			if (nums[mid] == target) {
				int start = mid;
				while (nums[start] == target) {
					start--;
				}
				int end = mid;
				while (nums[end] == target) {
					end++;
				}
				return new int[] { start + 1, end - 1 };
			} else if (nums[mid] < target) {
				left = mid + 1;
			} else {
				right = mid - 1;
			}
		}
		return new int[] { -1, -1 };
	}

}

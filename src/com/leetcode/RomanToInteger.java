package com.leetcode;

import java.util.HashMap;
import java.util.Map;

public class RomanToInteger {

	public static void main(String[] args) {

	}

	public static int romanToInt(String s) {
		Map<Character, Integer> valueMap = new HashMap<>();
		valueMap.put('I', 1);
		valueMap.put('V', 5);
		valueMap.put('X', 10);
		valueMap.put('L', 50);
		valueMap.put('C', 100);
		valueMap.put('D', 500);
		valueMap.put('M', 1000);
		int result = valueMap.get(s.charAt(s.length() - 1));
		int prev = result;
		for (int i = s.length() - 2; i >= 0; i--) {
			int value = valueMap.get(s.charAt(i));
			if (value < prev) {
				result -= value;
			} else {
				result += value;
			}
			prev = value;
		}
		return result;
	}

}

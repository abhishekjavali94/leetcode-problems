package com.leetcode;

public class IntegerToRoman {

	public static void main(String[] args) {
//		for (int i = 0; i < 4000; i++) {
//			System.out.println(i + " - " + intToRoman(i));
//		}
		int i = 89;
		System.out.println(i + " - " + intToRoman(i));

	}

	public static String intToRoman(int num) {
		StringBuilder revRoman = new StringBuilder();
		int digit = num % 10;
		revRoman.append(getRoman(digit, "I", "V", "X"));

		num = num / 10;
		digit = num % 10;
		revRoman.append(getRoman(digit, "X", "L", "C"));

		num = num / 10;
		digit = num % 10;
		revRoman.append(getRoman(digit, "C", "D", "M"));

		num = num / 10;
		digit = num % 10;
		revRoman.append(getRoman(digit, "M", "", ""));
		return revRoman.reverse().toString();
	}

	private static String getRoman(int digit, String a, String b, String c) {
		switch (digit) {
		case 1:
			return a;
		case 2:
			return a.concat(a);
		case 3:
			return a.concat(a).concat(a);
		case 4:
			return b.concat(a);
		case 5:
			return b;
		case 6:
			return a.concat(b);
		case 7:
			return a.concat(a).concat(b);
		case 8:
			return a.concat(a).concat(a).concat(b);
		case 9:
			return c.concat(a);
		}
		return "";
	}

}

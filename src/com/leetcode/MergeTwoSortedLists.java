package com.leetcode;

import static com.leetcode.Helper.insert;

import com.leetcode.Helper.ListNode;

public class MergeTwoSortedLists {

	public ListNode mergeTwoLists(ListNode l1, ListNode l2) {
		if (l1 == null) {
			return l2;
		}
		if (l2 == null) {
			return l1;
		}
		ListNode head = null;
		while (l1 != null || l2 != null) {
			if (l1 == null) {
				head = insert(head, l2.val);
				l2 = l2.next;
			} else if (l2 == null) {
				head = insert(head, l1.val);
				l1 = l1.next;
			} else if (l1.val <= l2.val) {
				head = insert(head, l1.val);
				l1 = l1.next;
			} else {
				head = insert(head, l2.val);
				l2 = l2.next;
			}
		}
		return head;
	}

}

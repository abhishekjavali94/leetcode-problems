package com.leetcode;

import java.util.ArrayList;
import java.util.List;

public class SpiralMatrix {

	public static void main(String[] args) {
		int[][] matrix = new int[][] { { 1, 2, 3, 4 }, { 5, 6, 7, 8 }, { 9, 10, 11, 12 } };
		System.out.println(spiralOrder(matrix));
	}

	public enum Operation {
		INCR, DECR, NOT;
	}

	public static List<Integer> spiralOrder(int[][] matrix) {
		List<Integer> result = new ArrayList<>();
		int i = 0, j = 0;
		int iL = 1, iU = matrix.length - 1;
		int jL = 0, jU = matrix[0].length - 1;
		int totalElem = matrix.length * matrix[0].length;
		Operation iOp = Operation.NOT;
		Operation jOp = Operation.INCR;
		result.add(matrix[0][0]);
		while (result.size() != totalElem) {
			if (jOp != Operation.NOT) {
				if (jOp == Operation.INCR) {
					if (j == jU) {
						jOp = Operation.NOT;
						jU--;
						iOp = Operation.INCR;
						i++;
					} else {
						j++;
					}
				} else {
					if (j == jL) {
						jOp = Operation.NOT;
						jL++;
						iOp = Operation.DECR;
						i--;
					} else {
						j--;
					}
				}
			} else {
				if (iOp == Operation.INCR) {
					if (i == iU) {
						iOp = Operation.NOT;
						iU--;
						jOp = Operation.DECR;
						j--;
					} else {
						i++;
					}
				} else {
					if (i == iL) {
						iOp = Operation.NOT;
						iL++;
						jOp = Operation.INCR;
						j++;
					} else {
						i--;
					}
				}
			}
			result.add(matrix[i][j]);
		}
		return result;
	}

}

package com.leetcode;

import java.util.ArrayList;
import java.util.List;

public class ZigZagConversion {

	public static void main(String[] args) {
		System.out.println(convert("PAYPALISHIRING", 3));
		System.out.println(convert("PAYPALISHIRING", 4));
		System.out.println(convert("A", 1));
		System.out.println(convert("AB", 1));
	}

	public static String convert(String s, int numRows) {
		if (numRows == 1 || s.length() == 1) {
			return s;
		}
		List<String> results = new ArrayList<>(numRows);
		for (int i = 0; i < numRows; i++) {
			results.add("");
		}
		int c = 0;
		boolean inc = true;
		for (int i = 0; i < s.length(); i++) {
			results.set(c, results.get(c).concat(String.valueOf(s.charAt(i))));
			if (inc) {
				c++;
			} else {
				c--;
			}
			if (c == numRows - 1) {
				inc = false;
			} else if (c == 0) {
				inc = true;
			}
		}
		String result = "";
		for (String str : results) {
			result = result.concat(str);
		}
		return result;
	}

}

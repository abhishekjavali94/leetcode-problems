package com.leetcode;

import java.util.Arrays;

import com.leetcode.Helper.ListNode;

public class ReverseLinkedList {

	public static void main(String[] args) {
		ListNode l = Helper.create(Arrays.asList(1, 2, 3, 4, 5));
		Helper.print(l);
		l = reverse(l);
		Helper.print(l);
	}

	public static ListNode reverse(ListNode head) {
		ListNode current = head;
		ListNode prev = null;

		while (current != null) {
			ListNode next = current.next;
			current.next = prev;
			prev = current;
			current = next;
		}
		return prev;
	}

}
